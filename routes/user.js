import { Router } from "express"
import jwt from "jsonwebtoken"
import UserDB from "../db/user"

const router = new Router()
router.post('/login', async (req, res) => {

    const { username, password } = req.body
    const userInfo = await UserDB.loginUser(username, password)

    if (!userInfo) {
        return res.json({
            error: true,
        })
    }

    const token = jwt.sign({
        username: username,
        password: password,
    }, 'secret', { expiresIn: "1h" })

    return res.json({
        error: false,
        token
    })
})

router.post('/register', async (req, res) => {
    const { username, password } = req.body
    if (!username.trim() || !password.trim()) {
        return { error: true }
    }

    const newUser = await UserDB.registerUser(username, password)
    return res.json({
        error: !!newUser,
        data: newUser,
    })
})

export default router;