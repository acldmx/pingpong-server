import { Router } from "express"
import { getChampionList } from "../db/champion"
import { getPlayerInfo } from "../db/pingscore"
import userRouter from "./user"
const router = new Router()

router.use('/', userRouter)

//获取冠军榜数据
router.get("/winner-list", async (req, res) => {
    const { name } = req.query
    const data = await getChampionList(name)
    return res.json(data)
})

router.get('/player', async (req, res) => {
    const { name } = req.query
    const data = await getPlayerInfo(name)
    return res.json(data)
})

export default router