import Sequelize from "sequelize"

/**
 * @param {Sequelize} sequelize
 */
export default sequelize => {
    return sequelize.define("champion", {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
        },
        name: {
            type: Sequelize.STRING(50),
            allowNull: false,
            defaultValue: '',
        },
        game: {
            type: Sequelize.STRING(100),
            allowNull: false,
            defaultValue: "",
        },
        group: {
            type: Sequelize.INTEGER,
            allowNull: false,
        },
        game_time: {
            type: Sequelize.DATE,
            allowNull: false,
        },
    })
}