import Sequelize from "sequelize"
import { DBConfig } from "../config"
const sequelize = new Sequelize(DBConfig.DBNAME, DBConfig.USERNAME, DBConfig.PASSWORD, {
    host: DBConfig.HOST,
    port: DBConfig.PORT,
    dialect: DBConfig.DIALECT,
    define: {
        timestamps: true,
        createdAt: false,
        updatedAt: false,
        deletedAt: false,
        //禁止修改表名
        freezeTableName: true,
    }
});

export default sequelize;