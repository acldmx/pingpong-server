import nedb from "nedb-async";

/**
 * @param {AsyncNedb} db
 */
const db = new nedb({
    filename: './user.db',
    autoload: true,
})

export default {
    async registerUser(username, password) {
        const alreadyUser = await db.asyncFindOne({ username })
        if (alreadyUser) {
            return null
        }

        const newUser = await db.asyncInsert({
            username,
            password,
        })

        return newUser
    },

    async loginUser(username, password) {
        const loginUser = await db.asyncFindOne({ username, password })
        return loginUser ? loginUser : null
    }
}