import db from "."
import PingScoreModel from "./pingscore.model"

const PingScore = PingScoreModel(db)

export async function getPlayerInfo(name) {
    return await PingScore.findOne({
        where: {name: name}
    })
}