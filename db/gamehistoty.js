import db from "./index"
// import GameHistoryModel from "./gamehistory.model"
// const GameHistory = GameHistoryModel(db)

export async function getWinnerList() {
    const [data, meta] = await db.query(`
            SELECT game_name, 
                SUBSTRING_INDEX(
                    GROUP_CONCAT(NAME ORDER BY score DESC),
                    ',',
                    1) name
            FROM game_history
            INNER JOIN ping_score
            ON game_history.game_id = ping_score.game_id 
            WHERE join_flag = 1
            GROUP BY game_name
        `)

    return data
}