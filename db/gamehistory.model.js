import Sequelize from "sequelize"
export default (sequelize) => {
    return sequelize.define('game_history', {
        game_time: {
            type: Sequelize.DATE,
            primaryKey: true,
        },
        game_name: {
            type: Sequelize.STRING,
            allowNull: true,
        },
        game_id: {
            type: Sequelize.INTEGER,
            allowNull: true,
        }
    })
}