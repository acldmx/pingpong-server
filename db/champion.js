import db from '.'
import championModel from "./champion.model"
import Sequelize from 'sequelize'
const Op = Sequelize.Op

const champion = championModel(db)
export async function getChampionList(name) {
    name = !!name ? name : ""
    const championList = await champion.findAll({
        where: {
            name: {
                [Op.like]: `%${name}%`
            }
        }
    })
    return championList
}