import Sequelize from "sequelize"
export default (sequelize) => {
    return sequelize.define('ping_score', {
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            primaryKey: true,
        },
        update_time: {
            type: Sequelize.DATE,
            allowNull: false,
            primaryKey: true,
        },
        add_time: {
            type: Sequelize.DATE,
            defaultValue: "2019-06-06 00:00:00"
        },
        score: {
            type: Sequelize.INTEGER,
        },
        change_score: {
            type: Sequelize.INTEGER,
        },
        game_count: {
            type: Sequelize.INTEGER,
            defaultValue: 0,
        },
        join_flag: {
            type: Sequelize.INTEGER,
            defaultValue: 0,
        },
        game_id: {
            type: Sequelize.INTEGER,
        }
    })
}
