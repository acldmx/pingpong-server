import express from "express"
import routes from "./routes"
import config from "./config"
import bodyParser from "body-parser"

const app = new express()

config(app)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/', routes)
app.get('/', (res, req) => {
    return req.json({
        "msg": "hello!",
    })
})

app.listen(8080, () => {
    console.log("server start!")
})
