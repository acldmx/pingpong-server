/**
 * 
 * @param {Express.Application} app 
 */
export default function (app) {

    app.all('*', function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "X-Requested-With");
        res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
        res.header("X-Powered-By", ' 3.2.1')
        res.header("Content-Type", "application/json;charset=utf-8");
        next();
    })

}

export const DBConfig = {
    DBNAME: 'ping',
    USERNAME: 'ping',
    PASSWORD: 'ping',
    HOST: 'xgrit.xicp.net',
    PORT: '13306',
    DIALECT: 'mysql',
}